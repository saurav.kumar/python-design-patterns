from Tests import *

class TestRunner:
    """
    Provide a unified interface to a set of interfaces in a subsystem.
    Facade defines a higher-level interface that makes the subsystem easier
    to use.
    """
    def __init__(self):
        self.test1 = Test1()
        self.test2 = Test2()
        self.test3 = Test3()

    def runTests(self):
        self.test1.run()
        self.test2.run()
        self.test3.run()
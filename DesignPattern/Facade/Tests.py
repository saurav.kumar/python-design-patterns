import time

class Test1:
    def run(self):
        print('Setup for Test Case 1...')
        time.sleep(1)
        print('Running Test Case 1...')
        time.sleep(1)
        print('Teardown for Test Case 1...')
        time.sleep(1)

class Test2:
    def run(self):
        print('Setup for Test Case 2...')
        time.sleep(1)
        print('Running Test Case 2...')
        time.sleep(1)
        print('Teardown for Test Case 2...')
        time.sleep(1)

class Test3:
    def run(self):
        print('Setup for Test Case 3...')
        time.sleep(1)
        print('Running Test Case 3...')
        time.sleep(1)
        print('Teardown for Test Case 3...')
        time.sleep(1)
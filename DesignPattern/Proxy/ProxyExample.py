from abc import ABC, abstractmethod

class Manager(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def work(self):
        pass

class RealManager(Manager):
    #Real Subject class
    def work(self):
            print('Great! THe manager is free to talk!!')

class Proxy(Manager):
    def __init__(self):
        self.is_busy = True

    def work(self):
        if not  self.is_busy:
            manager = RealManager()
            manager.work()
        else:
            print('Sorry, the manager is busy, please try after some time!!')

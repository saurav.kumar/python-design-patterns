from ProxyExample import *

if __name__ == '__main__':
    manager = Proxy()
    manager.work()


'''
source : http://www.bogotobogo.com/DesignPatterns/proxy.php
1. Both the Proxy and the RealSubject implement the Subject interface. This allows any client to treat the Proxy just like the RealSubject.
2. The RealSubject is usually the object that does most of the real work; the Proxy controls access to it.
3. The Proxy often instantiates or handle the creation of the RealSubject.
4. The Proxy keeps a reference (pointer) to the Subject, so it can forward requests to the Subject when necessary.
5. In some cases, the Proxy may be responsible for creating and destroying the RealSubject. Clients interact with the RealSubject through the Proxy.

The Proxy design pattern essentially does the following:
1. It provides a surrogate for another object so that you can control access to the original
object
2. It is used as a layer or interface to support distributed access
3. It adds delegation and protects the real component from undesired impact

TYPES :
Remote Proxy
With Remote Proxy, the proxy acts as a local representative for an object that lives in a different address space. 
A method call on the proxy results in the call being transferred over the wire, invoked remotely, and the result 
being returned back to the proxy and then to the client.

Virtual Proxy
With Virtual Proxy, the proxy acts as a representative for an object that may be expensive to create. 
The Virtual Proxy often defers the creation of the object until it is needed. 
The Virtual Proxy also act as a surrogate for the object before and while it is being created. After that, 
the proxy delegates requests directly to the RealSubject. (Above example is for Virtual Proxy)

Protection Proxy
With Protection Proxy, the proxy controls access to an object based on access rights. 
For instance, if we had an employee object, a Protection Proxy might allow the employee to call certain methods on the object, 
a manager to call additional methods (like makeBonus()), and a HR employee to call any method on the object.

A smart proxy
Smart proxies interpose additional actions when an object is accessed. 
For example,Instead of services directly invoking the real subject, a smart proxy is built-in and checks whether the
real object is locked or free before it is accessed in order to ensure that no other object can change it.

'''
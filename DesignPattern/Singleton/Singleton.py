'''
source : http://index-of.es/Varios-2/Learning%20Python%20Design%20Patterns.pdf

Singleton provides you with a mechanism to have one, and only one, object of a given
type and provides a global point of access. Hence, Singletons are typically used in cases
such as logging or database operations, printer spoolers, and many others, where there is a
need to have only one instance that is available across the application to avoid conflicting
requests on the same resource.

For example, we may want to use one database object to
perform operations on the DB to maintain data consistency or one object of the logging
class across multiple services to dump log messages in a particular log file sequentially.

In brief, the intentions of the Singleton design pattern are as follows:

-- Ensuring that one and only one object of the class gets created
-- Providing an access point for an object that is global to the program
-- Controlling concurrent access to resources that are shared

1. We will allow the creation of only one instance of the Singleton class.
2. If an instance exists, we will serve the same object again.

'''

class Singleton:
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Singleton, cls).__new__(cls)
            print('New object instance created.')
        else:
            print('Instance of Singleton class already exists, hence returning existing instance')

        return cls.instance

if __name__ == '__main__':
    s = Singleton()
    print("Object created", s)

    s2 = Singleton()
    print("Object created", s2)

#O/p -
# New object instance created.
# Object created <__main__.Singleton object at 0x00000000028C3BE0>
# Instance of Singleton class already exists, hence returning existing instance
# Object created <__main__.Singleton object at 0x00000000028C3BE0>

#Note the same object address , hence same instance is returned
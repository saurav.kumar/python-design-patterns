from abc import abstractmethod, ABC

class Animal(ABC):
    #Abstract Class
    @abstractmethod
    def make_sound(self):
        pass

class Dog(Animal):
    #Implements abstract class
    def make_sound(self):
        print('Dog is barking BOW BOW!!')

class Cat(Animal):
    #Implements abstract class
    def make_sound(self):
        print('Cat says MEOW MEOW!!')
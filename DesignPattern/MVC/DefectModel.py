import sqlite3

class StudentModel:
    def get_student_count(self):
        query = 'select count(*) from Student'
        count = self._dbselect(query)[0][0]
        return count

    def get_student_detail(self, name):
        query = 'select name, age from Student where name="{}"'.format(name)
        result = self._dbselect(query)
        return result

    def _dbselect(self, query):
        conn = sqlite3.connect('Student.db')
        cursorObj = conn.cursor()
        result =  cursorObj.execute(query).fetchall()
        conn.commit()
        conn.close()
        return result
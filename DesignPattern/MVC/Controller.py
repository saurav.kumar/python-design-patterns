from DefectModel import StudentModel
from DefectView import StudentView

class Controller:
    def __init__(self):
        self.model = StudentModel()
        self.view = StudentView()

    def getTotalCount(self):
        count = self.model.get_student_count()
        self.view.total_records(str(count))

    def getStudentDetail(self, name):
        result = self.model.get_student_detail(name)
        for record in result:
            self.view.details(record[0], record[1])
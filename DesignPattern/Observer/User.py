from TechForum import TechForum
from Subscriber import *

if __name__ == '__main__':
    publisher = TechForum()
    user1 = User1()
    user2 = User2()
    sites = SisterSites()

    publisher.register(user1)
    publisher.register(user2)
    publisher.register(sites)

    publisher.writeNewPost('Hello this is Saurav!')     #Notifies all users
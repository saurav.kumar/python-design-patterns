class Subscriber:
    def __init__(self):
        pass

    def notify(self, postname):
        pass


class User1(Subscriber):
    def notify(self, postname):
        print('User 1 notified for new post {}'.format(postname))

class User2(Subscriber):
    def notify(self, postname):
        print('User 2 notified for new post {}'.format(postname))

class SisterSites(Subscriber):
    def __init__(self):
        self.sites = ['abc', 'xyz', 'qwe']

    def notify(self, postname):
        for site in self.sites:
            print('Site {} notified for new post {}'.format(site, postname))
from Publisher import Publisher

class TechForum(Publisher):
    def __init__(self):
        self._listOfUsers = []
        self.postName = []

    def register(self, userObj):
        if userObj not in self._listOfUsers:
            self._listOfUsers.append(userObj)

    def unregister(self, userObj):
        if userObj in self._listOfUsers:
            self._listOfUsers.remove(userObj)

    def notifyAll(self):
        for userObj in self._listOfUsers:
            userObj.notify(self.postName)

    def writeNewPost(self, postname):
        self.postName = postname
        self.notifyAll()

